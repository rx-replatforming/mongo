#!/usr/bin/env bash
#
# Use this wrapper script as Docker entrypoint to set the UID and GID
# of user/group 'mongodb' in container to fix data files owner
#
# CAVEAT:
# - security risk if either value conflicts already in-use in the container
#
set -u
_uid="${MONGODB_UID:-}"
_gid="${MONGODB_GID:-$_uid}"
if [ -n "$_uid" ] ; then
    usermod -u $_uid mongodb
    groupmod -g $_gid mongodb
    # https://github.com/docker-library/mongo/blob/master/Dockerfile-linux.template
    chown -R -h ${_uid}:${_gid} /data/db /data/configdb /var/log/mongodb
    echo "mongodb UID/GID set" >&2
else
    echo "mongodb UID/GID unchanged" >&2
fi
# When $1=mongod, the default entrypoint script does something wonderful
if [ -z "${1:-}" ] || [ "${1:0:1}" = '-' ]; then
    set -- mongod "$@"
fi
# Pass control to the original entrypoint script
exec /usr/local/bin/docker-entrypoint.sh "$@"
