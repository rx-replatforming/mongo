FROM mongo
COPY set-mongodb-uid.sh /set-mongodb-uid.sh
ENTRYPOINT ["/set-mongodb-uid.sh"]
CMD ["mongod"]
