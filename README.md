# mongo

MongoDB based on the latest docker image on Docker Hub enhanced to run with custom uid/gid.

## Usage

Define environment variable *MONGODB_UID* and *MONGODB_GID* to override the default numeric UID/GID of `mongodb` in the container.

Other than that, consult upstream documentation for details, https://hub.docker.com/_/mongo
